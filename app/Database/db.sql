drop database if exists db;
create database db;
use db;

create table tuoteryhma(
    id int primary key auto_increment,
    nimi varchar(50) unique not null
);

insert into tuoteryhma (nimi) values ('asusteet');
insert into tuoteryhma (nimi) values ('urheiluvälineet');
insert into tuoteryhma (nimi) values ('puhelimet');