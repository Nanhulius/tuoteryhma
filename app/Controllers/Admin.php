<?php namespace App\Controllers;

use App\Models\TuoteryhmaModel;

class Admin extends BaseController
{
	public function index()
	{
        $tuoteryhmaModel = new TuoteryhmaModel();
        $data['tuoteryhmat'] = $tuoteryhmaModel -> haeTuoteryhmat();
        $data['otsikko'] = 'Tuoteryhmät';
		echo view('admin/tuoteryhma.php', $data);
    }
    
    public function tallenna() {
        $tuoteryhmaModel = new TuoteryhmaModel();
        if (!$this -> validate([
            'nimi' => 'required|max_length[50]'
        ])) {
            echo view('admin/tuoteryhma_lomake.php');
        } else {
            $talleta['nimi'] = $this -> request -> getVar('nimi');
            $tuoteryhmaModel ->save($talleta);
          /* $tuoteryhmaModel -> save([
               'nimi' => $this -> request -> getVar('nimi')
           ])*/
        }
        

    }
}